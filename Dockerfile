# Set the base image to node:alpine
FROM node:alpine as builder

# Specify where our app will live in the container
WORKDIR /app

# We only need the package.json to install dependencies.
# this will avoide dependency installation all the time
COPY package.json /app/

RUN npm install

# Copy the React App to the container
COPY . /app/

RUN npm run build

#Prepare nginx
FROM nginx:alpine
COPY --from=builder /app/build /usr/share/nginx/html
RUN rm /etc/nginx/conf.d/default.conf
COPY nginx/nginx.conf /etc/nginx/conf.d


# Fire up nginx
EXPOSE 80
CMD ["nginx", "-g", "daemon off;"]

